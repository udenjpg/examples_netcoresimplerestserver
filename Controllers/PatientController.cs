﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Logging;
using SimpleRestServer.FHIR;
using SimpleRestServer.Models;
using MongoDB.Bson;

namespace SimpleRestServer.Controllers
{
    [Route("api/[controller]")]
    public class PatientController : Controller
    {
        private MongoDataAccess objds;

        private readonly ILogger _logger;

        public PatientController(ILogger<PatientController> logger)
        {
            objds = new MongoDataAccess();
            _logger = logger;
        }

        // GET api/patient
        [HttpGet]
        public JsonResult Get()
        {
            _logger.LogWarning("Getting all values");
            IEnumerable<Patient> patients = objds.GetPatients();
             return Json(patients);
        }

        // GET api/patient/5
        [HttpGet("{id}")]
        public JsonResult  Get(string id)
        {
            List<Patient> patients = objds.GetPatients(id);
            _logger.LogWarning("Getting {id}:{patient}",id,patients.Count);

            return Json(patients);
        }

        // GET api/patient/ID/12345
        [HttpGet("{system}/{id}")]
        public JsonResult  Get(string system,string id)
        {
            List<Patient> patients = objds.GetPatients(id,system);
            _logger.LogWarning("Getting {id}:{patient}",id,patients.Count);

            return Json(patients);
        }

        // POST api/patient
        [HttpPost]
        public void Post([FromBody]string value)
        {

        }

        // PUT api/patient/5
        [HttpPut("{system}/{id}")]
        public void Put(string system, string id, [FromBody]string value)
        {
        }

        // DELETE api/patient/5
        [HttpDelete("{system}/{id}")]
        public void Delete(string system, string id)
        {
        }
    }
}
