﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Logging;
using SimpleRestServer.FHIR;
using SimpleRestServer.Models;
using MongoDB.Bson;

namespace SimpleRestServer.Controllers
{
    [Route("api/[controller]")]
    public class PopulateController : Controller
    {
        private MongoDataAccess objds;

        private readonly ILogger _logger;
        private static Random dummyRand = new Random();
        private static DateTime dummyDate =new DateTime(1920,01,01);

        public PopulateController(ILogger<PopulateController> logger)
        {
            _logger = logger;
            objds = new MongoDataAccess();

            dummyRand = new Random();
            dummyDate= new DateTime(1920,01,01);
        }

        // GET api/populate/5
        [HttpGet("{number}")]
        public JsonResult  Get(int number)
        {
            Task.Run(() => addDummyPatients(number));
            return Json(number);
        }

        private void addDummyPatients(int number) {
            List<Patient> patients = new List<Patient>();
            objds.Clean();
            for (int i = 0; i < number; i++)
            {
                patients.Add(dummyPatient());
                if (patients.Count==10000) {
                    objds.CreateBatch(patients);
                    patients = new List<Patient>();
                }
            }
            if (patients.Count>0) {
                objds.CreateBatch(patients);
            }
        }

        public static Patient dummyPatient() {
          String[] firstNames = {"Jane","John","Jan","Jannie","Joop","Henk","Bob"};
          String[] lastNames = {"Do","met de Pet","de Haas","DeBouwer"};

          int range = ((TimeSpan)(DateTime.Today - dummyDate)).Days;
          DateTime birthdate = dummyDate.AddDays(dummyRand.Next(range));
          Patient pat = new Patient {
            firstname=firstNames.ElementAt(dummyRand.Next(firstNames.Count())),
            lastname=lastNames.ElementAt(dummyRand.Next(lastNames.Count())),
            birthdate=birthdate
          };

          pat.identifiers.Add(new Identifier {system="ID",identifier=(dummyRand.Next(1111,9999)+10000).ToString()});
          pat.identifiers.Add(new Identifier {system="ZIS",identifier = (dummyRand.Next(111111111,999999999)+1000000000).ToString()});
          pat.identifiers.Add(new Identifier {system="HIS",identifier=(dummyRand.Next(1111,9999)+10000).ToString()});
          pat.identifiers.Add(new Identifier {system="GUID",identifier=Guid.NewGuid().ToString("D")});
          return pat;
        }
    }
}
