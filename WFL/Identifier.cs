using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace SimpleRestServer.WFL {
public class Identifier {
        public string value;
        public IdentifierOrigin format; 

        public bool isValid() {
            if (format!=null) {
            return Regex.IsMatch(value,format.pattern);
            } else {
                return false;
            }
        }
}

public class IdentifierOrigin {

  public string name;
  public string description;
  public string pattern;

}
}