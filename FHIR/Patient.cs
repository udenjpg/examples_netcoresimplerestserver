using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;

namespace SimpleRestServer.FHIR
{
  public class Patient
    {
        public ObjectId _id;
        public string firstname;
        public string lastname;
        public DateTime birthdate;
        public List<Identifier> identifiers;
        public Patient() {
            identifiers = new List<Identifier>();
        }
    }
}