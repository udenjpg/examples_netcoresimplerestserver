using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;

namespace SimpleRestServer.Models
{
    public class MongoDataAccess
    {
        //Mongo instances
        MongoClient _client;
        IMongoDatabase _db;
        IMongoCollection<FHIR.Patient> _patients;
        static string _PATIENTCOLLECTION = "Patients";
        string _MongoConnectionString ="@cluster0-shard-00-00-rykeu.mongodb.net:27017,cluster0-shard-00-01-rykeu.mongodb.net:27017,cluster0-shard-00-02-rykeu.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin";
        string _MongoUser = "CSharpUser:2X4BBGJnrfZbl2AJ";

        public MongoDataAccess()
        {
            _client = new MongoClient(string.Concat("mongodb://",_MongoUser,_MongoConnectionString));
            _db = _client.GetDatabase("Clinical");
            _patients = _db.GetCollection<FHIR.Patient>(_PATIENTCOLLECTION);
        }

        public void Clean()
        {
            _db.DropCollection(_PATIENTCOLLECTION);
            _db.CreateCollection(_PATIENTCOLLECTION);

            _patients.Indexes.CreateOne(Builders<FHIR.Patient>.IndexKeys.Ascending(_ => _.lastname));

            var filter = new BsonDocument{{"identifiers.identifier",1},{"identifiers.system",1}};
            _patients.Indexes.CreateOne(filter);
        }

        public IEnumerable<FHIR.Patient> GetPatients()
        {
            var filter = new BsonDocument();
            return _patients.Find(filter).Limit(100).ToList();
        }

        public List<FHIR.Patient> GetPatients(string searchForID)
        {
            var result = new List<FHIR.Patient>();
            var filter = new BsonDocument("identifiers.identifier", searchForID);
            return _patients.FindSync(filter).ToList();
        }

        public List<FHIR.Patient> GetPatients(string searchForID,string system)
        {
            var result = new List<FHIR.Patient>();
            var filterA = Builders<FHIR.Patient>.Filter.ElemMatch(r => r.identifiers, g => g.identifier == searchForID && g.system == system);
            return _patients.FindSync(filterA).ToList();
        }

        public void CreateBatch(List<FHIR.Patient> patients) {
            _patients.InsertMany(patients);
        }

        public FHIR.Patient Create(FHIR.Patient patient)
        {
            _patients.InsertOne(patient);
            return patient;
        }

        public void Update(string searchForID,string system,FHIR.Patient patient)
        {
            //var res = Query<FHIR.Patient>.EQ(patient => patient.identifier,searchForID);
            //var operation = Update<FHIR.Patient>.Replace(p);
            //_db.GetCollection<FHIR.Patient>("Patients").UpdateOne(res,operation);
        }
        public void Remove(string searchForID)
        {
            //var res = Query<FHIR.Patient>.EQ(patient => patient.identifier, searchForID);
            //var operation = _db.GetCollection<FHIR.Patient>("Patients").Remove(res);
        }
    }
}